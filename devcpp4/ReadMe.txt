- Bloodshed Dev-C++ 4 -

* What is Dev-C++ ?

Dev-C++ is a full-featured integrated development environment (IDE), which is able to create Windows or dos-based C/C++ programs using the Mingw compiler system (included with this package), or the Cygwin compiler.

* License

Dev-C++ is distributed under the GNU General Public License (see Copying.txt). It is freely distributable.

* Installing :

Be sure to uninstall any previous version of Dev-C++ before installing.

Run Setup.exe and follow the instruction.

if you are missing WININET.DLL on your Windows 95 system, you can      download it at: http://www.rocketdownload.com/supfiles.htm

* Help

You will find help in the Help menu of Dev-C++. There are some help files for you to learn how to use it. If you encounter problem, you can take a look at the Dev-C++ about box.
  
Good programming ;-)
Colin Laplace
webmaster@bloodshed.net