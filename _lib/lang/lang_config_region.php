<?php
  $this->Nm_lang_conf_region = array();
  $this->Nm_lang_conf_region['pt_br;pt_br'] = "Português (Brasil)";
  $this->Nm_lang_conf_region['de;de_de'] = "Alemão (Alemanha)";
  $this->Nm_lang_conf_region['es;es_es'] = "Espanhol (Espanha)";
  $this->Nm_lang_conf_region['en_us;en_us'] = "Inglês (Estados Unidos)";
  ksort($this->Nm_lang_conf_region);
?>